import sys
from PySide6.QtWidgets import QApplication
from my_widget import MyWidget



if __name__ == "__main__":
    app = QApplication([])

    widget = MyWidget()
    widget.resize(800, 600)
    widget.show()

    sys.exit(app.exec())